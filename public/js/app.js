
/* not in use, yet
Vue.component('odd-inline', {
  props: ['odds'],
  template: '<p><slot></slot></p>'
})

Vue.component('odd-list', {
  props: ['odds'],
  template: '<div><ul><li v-for="odd in odds">{{odd.description}}</li></ul></div>'
})

Vue.component('odds', {
  props: ['list'],
  render: function (createElement) {
    if (this.list.length > 3) {
      return createElement('div', [
        createElement('odd-inline', this.$slots.default)
      ])
    } else {
      return createElement('div', [
        createElement('odd-list', this.$slots.default)
      ])
    }
  },
  data () {
    return {
      result: ''
    }
  }
})
*/
var app = new Vue({
  el: '#myApp',
  http: {
    headers: {

    }
  },
  data: {
    showOdds: false,
    isList: false,
    myEvents: {},
    myBookings: [],
    selectedEvent: {},
    booking: {
      myEvent: '',
      odd: ''
    },
    renderingTemplate: '',
    stake: 0,
    winnings: 0,
    // "production" settings
    apiUrl: 'https://cors-anywhere.herokuapp.com/https://stanleybettechtestapi.herokuapp.com/available'
    // for test
    // apiUrl: 'data.json'
  },
  methods: {
    selectEvent: function (myEvent) {
      this.selectedEvent = myEvent
      this.renderingTemplate = myEvent.odds.length > 3 ? 'towin' : '1x2'
      this.selectedEvent.renderingTemplate = this.renderingTemplate
      this.updateListStatus()
      this.showOdds = true
    },
    getResourceFromApi: function () {
      this.$http.get(this.apiUrl).then((response) => {
        this.myEvents = response.body
      }).catch((response) => {
        console.log('Something went wrong...')
        console.error(response)
      })
    },
    updateListStatus: function () {
      this.isList = this.renderingTemplate === 'towin' ? true : false
      return true
    },
    bookEvent: function (myEvent, odd) {
      var b = {myEvent: myEvent, odd: odd}
      this.myBookings.push(b)
    },
    removeBooking: function (booking) {
      var index = this.myBookings.indexOf(booking)
      this.myBookings.splice(index, 1)
    },
    resetEvents: function () {
      this.myBookings = []
      this.stake = 0
      this.winnings = 0
    }
  },
  computed: {
    calculateWin: function () {
      let w = 0
      for (booking of this.myBookings) {
        w += Number(booking.odd.price) * Number(this.stake) + Number(this.stake)
      }
      return w
    }
  }
})

app.getResourceFromApi()
